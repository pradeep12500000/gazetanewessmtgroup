package com.microvate.gazetanewes.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.microvate.gazetanewes.home.HomeActivity;
import com.microvate.gazetanewes.R;

public class SplashActivity extends AppCompatActivity {

    public static final int TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashActivity.this,HomeActivity.class));
                    finish();
                    }
        },TIME_OUT);
    }
}
