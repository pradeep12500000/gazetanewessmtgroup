package com.microvate.gazetanewes.home.menu;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.microvate.gazetanewes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AudioActivity extends AppCompatActivity {

    @BindView(R.id.imageView_back)
    ImageView imageViewBack;
    @BindView(R.id.Linearlayout_radio)
    LinearLayout LinearlayoutRadio;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.Linearlayout_ENTRENIMENTO)
    LinearLayout LinearlayoutENTRENIMENTO;
    private boolean isPlay = false;
    private MediaPlayer mediaPlayer;
    @BindView(R.id.imageView_play)
    ImageView imageViewPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        ButterKnife.bind(this);
        mediaPlayer = MediaPlayer.create(this, R.raw.songs);

    }

    @OnClick({R.id.imageView_back, R.id.imageView_play})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                onBackPressed();
                break;
            case R.id.imageView_play:
                if (isPlay) {
                    isPlay = false;
                    mediaPlayer.pause();
                    imageViewPlay.setImageResource(R.drawable.icon_play);
                } else {
                    mediaPlayer.start();
                    isPlay = true;
                    imageViewPlay.setImageResource(R.drawable.icon_stop);
                }
                break;
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
    }
}