package com.microvate.gazetanewes.home.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.microvate.gazetanewes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactActivity extends AppCompatActivity {

    @BindView(R.id.imageView_back)
    ImageView imageViewBack;
    @BindView(R.id.Linearlayout_radio)
    LinearLayout LinearlayoutRadio;
    @BindView(R.id.imageView_logo)
    ImageView imageViewLogo;
    @BindView(R.id.Linearlayout_Audio)
    LinearLayout LinearlayoutAudio;
    @BindView(R.id.Linearlayout_ENTRENIMENTO)
    LinearLayout LinearlayoutENTRENIMENTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imageView_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
