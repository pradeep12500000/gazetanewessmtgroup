package com.microvate.gazetanewes.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.microvate.gazetanewes.home.menu.AudioActivity;
import com.microvate.gazetanewes.home.menu.ContactActivity;
import com.microvate.gazetanewes.home.menu.ENTRENIMENTO_Activtiy;
import com.microvate.gazetanewes.home.menu.Facebook_Activty;
import com.microvate.gazetanewes.home.menu.IMIGRACAO_Activity;
import com.microvate.gazetanewes.home.menu.JORNAL_IMPRESSO_Activity;
import com.microvate.gazetanewes.R;
import com.microvate.gazetanewes.home.menu.NOTICIAS_Activity;
import com.microvate.gazetanewes.home.menu.SOCIAIS_Activity;
import com.microvate.gazetanewes.home.menu.TV_Activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.Linearlayout_radio)
    LinearLayout LinearlayoutRadio;
    @BindView(R.id.Linearlayout_Audio)
    LinearLayout LinearlayoutAudio;
    @BindView(R.id.Linearlayout_ENTRENIMENTO)
    LinearLayout LinearlayoutENTRENIMENTO;
    @BindView(R.id.Linearlayout_JORNAL_IMPRESSO)
    LinearLayout LinearlayoutJORNALIMPRESSO;
    @BindView(R.id.Linearlayout_Facebook)
    LinearLayout LinearlayoutFacebook;
    @BindView(R.id.Linearlayout_Contact)
    LinearLayout LinearlayoutContact;
    @BindView(R.id.Linearlayout_IMIGRACAO)
    LinearLayout LinearlayoutIMIGRACAO;
    @BindView(R.id.Linearlayout_SOCIAIS)
    LinearLayout LinearlayoutSOCIAIS;
    @BindView(R.id.Linearlayout_NOTICIAS)
    LinearLayout LinearlayoutNOTICIAS;
    @BindView(R.id.Linearlayout_TV)
    LinearLayout LinearlayoutTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.Linearlayout_radio, R.id.Linearlayout_Audio, R.id.Linearlayout_ENTRENIMENTO, R.id.Linearlayout_JORNAL_IMPRESSO, R.id.Linearlayout_Facebook, R.id.Linearlayout_Contact, R.id.Linearlayout_IMIGRACAO, R.id.Linearlayout_SOCIAIS, R.id.Linearlayout_NOTICIAS, R.id.Linearlayout_TV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Linearlayout_radio:
                break;
            case R.id.Linearlayout_Audio:
                startActivity(new Intent(HomeActivity.this, AudioActivity.class));
                break;
            case R.id.Linearlayout_ENTRENIMENTO:
                startActivity(new Intent(HomeActivity.this, ENTRENIMENTO_Activtiy.class));
                break;
            case R.id.Linearlayout_JORNAL_IMPRESSO:
                startActivity(new Intent(HomeActivity.this, JORNAL_IMPRESSO_Activity.class));
                break;
            case R.id.Linearlayout_Facebook:
                startActivity(new Intent(HomeActivity.this, Facebook_Activty.class));
                break;
            case R.id.Linearlayout_Contact:
                startActivity(new Intent(HomeActivity.this, ContactActivity.class));
                break;
            case R.id.Linearlayout_IMIGRACAO:
                startActivity(new Intent(HomeActivity.this, IMIGRACAO_Activity.class));
                break;
            case R.id.Linearlayout_SOCIAIS:
                startActivity(new Intent(HomeActivity.this, SOCIAIS_Activity.class));
                break;
            case R.id.Linearlayout_NOTICIAS:
                startActivity(new Intent(HomeActivity.this, NOTICIAS_Activity.class));
                break;
            case R.id.Linearlayout_TV:
                startActivity(new Intent(HomeActivity.this, TV_Activity.class));
                break;
        }
    }
}
